# files2datedirs

!!! tip "What this script does..."
	creates directories from date of files, then moves files to these directories

- [source code](https://gitlab.com/ekleinod/bash-scripts/-/blob/main/files/files2datedirs)

## Synopsis

``` shell
files2datedirs
files2datedirs [-d date-provider] [-s suffix] [-h]
files2datedirs [OPTION]
```

## Parameters

-d
: date provider, `filename` or `date`, default `date`

-s
: directory suffix, default `_Miezen`

-h
: show help

## Description

This script creates directories from the date (file date or filename) of files in the type and year subdirectories, then moves the files to these directories.
It differs between types images (`jpg`) and movies (`mp4 mov`).
The type subdirectories are `Fotos` and `Videos` resp.
The default suffix for the date directories is `_Miezen`, this results in e.g. `Fotos/2022/2022-02-28_Miezen`.

## Examples

``` shell
$ files2datedirs
$ files2datedirs -d filename
$ files2datedirs -s "_Tierpark"
$ files2datedirs -h
```
