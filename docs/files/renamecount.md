# renamecount

!!! warning
	this script needs more tests, make backups before use...

!!! tip "What this script does..."
	rename files using a counter (recursively)

- [source code](https://gitlab.com/ekleinod/bash-scripts/-/blob/main/files/renamecount)

## Synopsis

``` shell
renamecount DIRECTORY
renamecount DIRECTORY [FILES] [PATTERN]
renamecount [OPTION]
```

## Parameters

-h, --help
: show help

## Description

This script renames all files in the directory and all subdirectories using a counter.

The original images are renamed.
Possible overrides prompt a manual confirmation.

## Examples

``` shell
$ renamecount myfiles
$ renamecount myfiles "*.jpg"
$ renamecount myfiles "*.jpg" "%s_%05d_copy-%s.jpg"
```
