# mkdirfromfiles

!!! tip "What this script does..."
	create directories from filenames

- [source code](https://gitlab.com/ekleinod/bash-scripts/-/blob/main/files/mkdirfromfiles)

## Synopsis

``` shell
mkdirfromfiles [ADDENDUM]
mkdirfromfiles [ADDENDUM] [FILES]
mkdirfromfiles [OPTION]
```

## Parameters

-h, --help
: show help

## Description

This script creates directories from all files of the given pattern in the directory, adding an addendum to the filename.

Existing directories are left untouched.

## Examples

``` shell
$ mkdirfromfiles
$ mkdirfromfiles "__"
$ mkdirfromfiles "__" "*.md"
```
