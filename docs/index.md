# Introduction

![edge-soft logo](assets/icon.png){ align=right }

!!! tip ""
	Automate your life!

This is a collection of useful bash scripts.
They are designed to fulfil a special need of mine.

As bash scripting is very hard (in my opinion), the scripts provide some real life examples of using bash scripts for traversing a directory etc.

I am trying to document the scripts, if you have questions, or find errors feel free to provide an issue:

- [issues](https://gitlab.com/ekleinod/bash-scripts/-/issues)

If you are interested in the git workflow, please see the [workflow documentation of edgeutils](https://ekleinod.gitlab.io/edgeutils/30-git-workflow/).

## License

These scripts are distributed under the terms of the GNU General Public License.

See [readme](https://gitlab.com/ekleinod/bash-scripts/-/blob/main/README.md) or [COPYING](https://gitlab.com/ekleinod/bash-scripts/-/blob/main/COPYING) for details.
