# git-update-all

!!! tip "What this script does..."
	updates all git repositories in current directory and all subdirectories

- [source code](https://gitlab.com/ekleinod/bash-scripts/-/blob/main/repos/git-update-all)


## Synopsis

~~~ shell
git-update-all
git-update-all [OPTION]
~~~

## Parameters

-h, --help
: show help

## Description

This script updates all git repositories in current directory and all subdirectories.

## Examples

~~~ shell
$ git-update-all
~~~
