# git-status-all

!!! tip "What this script does..."
	show status of all git repositories in current directory and all subdirectories

- [source code](https://gitlab.com/ekleinod/bash-scripts/-/blob/main/repos/git-status-all)


## Synopsis

~~~ shell
git-status-all
git-status-all [OPTION]
~~~

## Parameters

-h, --help
: show help

## Description

This script shows the status of all git repositories in current directory and all subdirectories.

## Examples

~~~ shell
$ git-status-all
~~~
