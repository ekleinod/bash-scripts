# image2gif

!!! tip "What this script does..."
	creates animated gif from images

- [source code](https://gitlab.com/ekleinod/bash-scripts/-/blob/main/stop-motion/image2gif)


## Synopsis

``` shell
image2gif
image2gif [OPTIONS]
```

## Parameters

-o
: output filename, default 'loop.gif'

-i
: image files, default '*.jpg'

-d
: delay per image in 1/100 s, default 20

-l
: number of loops, 0 = endless, default 0

-h
: show help

## Description

This script creates an animated gif from a bunch of images.

!!! tip
	Resize your images before creating an animated gif - saves an enormous amount of time.


## Examples

``` shell
$ image2gif
$ image2gif -o ../loop_600x400.gif -d 15
$ image2gif -l 1
```

## Prerequisites

`convert`
: - part of ImageMagick
	- <https://imagemagick.org/>
