# image2mp4

!!! tip "What this script does..."
	creates mp4 film from images

- [source code](https://gitlab.com/ekleinod/bash-scripts/-/blob/main/stop-motion/image2mp4)


## Synopsis

``` shell
image2mp4
image2mp4 [OPTIONS]
```

## Parameters

-o
: output filename, default 'film.mp4'

-i
: image files, default '*.jpg'

-f
: framerate, default 12

-h
: show help

## Description

This script creates an mp4 film from a bunch of images.

!!! tip
	Resize your images before creating a film - saves an enormous amount of time.


## Examples

``` shell
$ image2mp4
$ image2mp4 -o ../timelapse_1620x1080.mp4 -f 15
```

## Prerequisites

`ffmpeg`
: - cross-platform multimedia framework
	- <https://www.ffmpeg.org>
