# autorothere

!!! tip "What this script does..."
	rotates all image files (jpg) recursively

- [source code](https://gitlab.com/ekleinod/bash-scripts/-/blob/main/images/autorothere)


## Synopsis

``` shell
autorothere
autorothere [OPTION]
```

## Parameters

-h, --help
: show help

## Description

This script rotates all image files (jpg) in the containing directory and all subdirectories recursively according to the EXIF information.

The original images are overwritten.

## Examples

``` shell
$ autorothere
```

## Prerequisites

`jhead`
: - officially in ubuntu universe
	- <https://www.sentex.ca/~mwandel/jhead/>

`libjpeg-progs` or `libjpeg-turbo-progs`
: - officially in ubuntu universe
	- *turbo* is faster
	- otherwise you get the error: "jpegtran: not found"
