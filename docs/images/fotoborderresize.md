# fotoborderresize

!!! tip "What this script does..."
	resize all image files with border, if neccessary

- [source code](https://gitlab.com/ekleinod/bash-scripts/-/blob/main/images/fotoborderresize)


## Synopsis

``` shell
fotoborderresize
fotoborderresize [SIZE]
fotoborderresize [OPTION]
```

## Parameters

SIZE
: new image size, default = 1920x1080

-h, --help
: show help

## Description

This script resizes all image files in the directory proportional to 1920x1080, or the given size.

The original images are overwritten.
If one size (width or height) is to small, a black border is generated.
Thus, all images have the same size.

Optionally an image size can be given.

This one is useful if all images have to be of the same size, because the viewer does not resize the images or does not create borders if one size does not fit.

## Examples

``` shell
$ fotoborderresize
$ fotoborderresize 1280x720
```

## Prerequisites

`convert`
: - part of ImageMagick
	- <https://imagemagick.org/>
