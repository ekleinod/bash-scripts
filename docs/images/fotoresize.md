# fotoresize

!!! tip "What this script does..."
	resize all image files  in the directory proportionally to the given size

- [source code](https://gitlab.com/ekleinod/bash-scripts/-/blob/main/images/fotoresize)


## Synopsis

``` shell
fotoresize
fotoresize [SIZE]
fotoresize [OPTION] [SIZE]
```

## Parameters

SIZE
: new image size, default = 1920x1080

-v, --verbose
: show resized file names

-h, --help
: show help

## Description

This script resizes all image files in the directory proportional to 1920x1080, or the given size.

The original images are overwritten.

The `-v` flag allows for output of the resized images.

## Examples

``` shell
$ fotoresize
$ fotoresize -v
$ fotoresize 600x400
$ fotoresize --verbose 600x400
```

## Prerequisites

`convert`
: - part of ImageMagick
	- <https://imagemagick.org/>
