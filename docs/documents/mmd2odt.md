# mmd2odt

!!! tip "What this script does..."
	converts a multimarkdown file to odt.

- [source code](https://gitlab.com/ekleinod/bash-scripts/-/blob/main/documents/mmd2odt)

## Synopsis

~~~ shell
mmd2odt MARKDOWNFILE
mmd2odt [OPTION]
~~~

## Parameters

MARKDOWNFILE
: file to convert to odt, with or without extension

-h
: show help

## Description

This script converts a multimarkdown file to odt ([LibreOffice Writer](https://www.libreoffice.org)) using multimarkdown and pdfLaTeX.

If no extension is given the script adds `.md` and `.mmd` to the filename and uses this file if it exists.
The same goes for files with another extension.

## Requisites

The script uses [MultiMarkdown](https://fletcher.github.io/MultiMarkdown-6/).

## Examples

~~~ shell
$ mmd2odt myfile
$ mmd2odt myfile.md
$ mmd2odt myfile.mmd
~~~
