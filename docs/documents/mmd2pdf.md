# mmd2pdf

!!! tip "What this script does..."
	converts a multimarkdown file to pdf.

- [source code](https://gitlab.com/ekleinod/bash-scripts/-/blob/main/documents/mmd2pdf)

## Synopsis

~~~ shell
mmd2pdf MARKDOWNFILE
mmd2pdf [OPTION]
~~~

## Parameters

MARKDOWNFILE
: file to convert to pdf, with or without extension

-h
: show help

## Description

This script converts a multimarkdown file to pdf using multimarkdown and pdfLaTeX.

If no extension is given the script adds `.md` and `.mmd` to the filename and uses this file if it exists.
The same goes for files with another extension.

## Requisites

The script uses [MultiMarkdown](https://fletcher.github.io/MultiMarkdown-6/) and pdfLaTeX.

## Examples

~~~ shell
$ mmd2pdf myfile
$ mmd2pdf myfile.md
$ mmd2pdf myfile.mmd
~~~
