# ocr

!!! tip "What this script does..."
	Runs ocr on one file or all pdf files in the directory.

- [source code](https://gitlab.com/ekleinod/bash-scripts/-/blob/main/documents/ocr)

## Synopsis

~~~ shell
ocr
ocr [-i inputfile] [-o outputfile] [-l language]
ocr [OPTION]
~~~

## Parameters

-i
: file to run ocr on, default: all files in the directory

-o
: output file, default inputfile with "-ocr" added to name

-l
: language to use for ocr, default "deu"

-h
: show help

## Description

This script runs ocr on one file or all pdf files in the directory.

## Requisites

The script uses [ocrmypdf](https://github.com/ocrmypdf/OCRmyPDF) as ocr program.

You can install it directly or use [docker](https://gitlab.com/etg-docker/docker-commands/-/blob/main/direct_calls/ocrmypdf-docker).

*ocrmypdf* uses [tesseract](https://github.com/tesseract-ocr) for languages, see [available languages here](https://github.com/tesseract-ocr/tessdata).

## Examples

~~~ shell
$ ocr
$ ocr -l eng
$ ocr -i myfile.pdf
$ ocr -i myfile.pdf -o  myfile-ocred.pdf
~~~
