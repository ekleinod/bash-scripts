# v1.0, 2024-07-04
# Ekkart Kleinod (https://www.edgesoft.de/)

# defaults
DEFAULT_DATE_PROVIDER="date"

DEFAULT_EXTENSIONS_IMAGE="jpg"
DEFAULT_EXTENSIONS_MOVIE="mp4 mov"

DEFAULT_PATH_OUT_IMAGES="Fotos"
DEFAULT_PATH_OUT_MOVIES="Videos"

DEFAULT_PATH_SUFFIX="_Miezen"

# help
function showHelp() {
	echo "NAME"
	echo -e "\tfiles2datedirs - creates directories from date of files, then moves files to these directories"
	echo
	echo "SYNOPSIS"
	echo -e "\tfiles2datedirs"
	echo -e "\tfiles2datedirs [-d date-provider] [-s suffix] [-h]"
	echo -e "\tfiles2datedirs [OPTION]"
	echo
	echo "DESCRIPTION"
	echo -e "\tThis script creates directories from the date (file date or filename) of files in the type and year subdirectories, then moves the files to these directories."
	echo -e "\tIt differs between types images ($DEFAULT_EXTENSIONS_IMAGE) and movies ($DEFAULT_EXTENSIONS_MOVIE)."
	echo -e "\tThe type subdirectories are '$DEFAULT_PATH_OUT_IMAGES' and '$DEFAULT_PATH_OUT_MOVIES' resp."
	echo -e "\tThe default suffix for the date directories is '$DEFAULT_PATH_SUFFIX', this results in e.g. '$DEFAULT_PATH_OUT_IMAGES/2022/2022-02-28$DEFAULT_PATH_SUFFIX'."
	echo -e "\tSee https://ekleinod.gitlab.io/bash-scripts/files/files2datedirs/ for more information"
	echo
	echo -e "\t-d\tdate provider, 'filename' or 'date', default '$DEFAULT_DATE_PROVIDER'"
	echo -e "\t-s\tdirectory suffix, default '$DEFAULT_PATH_SUFFIX'"
	echo -e "\t-h\tshow this help"
	echo
}

# defaults
DATE_PROVIDER=$DEFAULT_DATE_PROVIDER
EXTENSIONS_IMAGE="$DEFAULT_EXTENSIONS_IMAGE"
EXTENSIONS_MOVIE="$DEFAULT_EXTENSIONS_MOVIE"
PATH_OUT_IMAGES=$DEFAULT_PATH_OUT_IMAGES
PATH_OUT_MOVIES=$DEFAULT_PATH_OUT_MOVIES
PATH_SUFFIX=$DEFAULT_PATH_SUFFIX

# parameters
while getopts hd:s: option
do
	case "${option}"
		in
			d)
				DATE_PROVIDER=${OPTARG}
				;;
			s)
				PATH_SUFFIX=${OPTARG}
				;;
			h | *)
				showHelp
				exit 0
				;;
	esac
done

if [[ $DATE_PROVIDER != "filename" ]]
then
	DATE_PROVIDER=$DEFAULT_DATE_PROVIDER
fi

echo "files2datedirs: processing files '$EXTENSIONS_IMAGE $EXTENSIONS_MOVIE' to '$PATH_OUT_IMAGES' and '$PATH_OUT_MOVIES' using '$DATE_PROVIDER' as date provider."

# move function
function createdirandmovefile {

	FILETYPE="${1}"
	OUTPATH="${2}"

	for file in *.$FILETYPE
	do

		if [[ -f "$file" ]]
		then

			# get file date
			filedate=

			if [[ $DATE_PROVIDER = "date" ]]
			then
				filedate=$(date -r $file -u +"%Y-%m-%d")
			else
				filedate=`expr "$file" : "\([[:digit:]][[:digit:]][[:digit:]][[:digit:]]-[[:digit:]][[:digit:]]-[[:digit:]][[:digit:]]\).*"`
			fi

			if [[ -z "$filedate" ]]
			then
				echo "Could not compute date of the file '$file'."
				exit 1
			fi

			year=`expr "$filedate" : "\([[:digit:]][[:digit:]][[:digit:]][[:digit:]]\).*"`

			# create directory
			outdir=$OUTPATH/$year/$filedate$PATH_SUFFIX
			if [[ ! -d $outdir ]]
			then
				mkdir --parent $outdir
			fi

			# move file
			echo "$file -> $outdir"
			mv $file $outdir

		fi

	done

}


for filetype in $EXTENSIONS_IMAGE
do
	createdirandmovefile "$filetype" "$PATH_OUT_IMAGES"
done

for filetype in $EXTENSIONS_MOVIE
do
	createdirandmovefile "$filetype" "$PATH_OUT_MOVIES"
done

# EOF
