# bash scripts

This is a collection of useful bash scripts.

Please feel free to use them, to adapt them, whatever you want.

Every script is documented in the [manual](https://ekleinod.gitlab.io/bash-scripts), so please read it.

This repository will not have a complicated workflow, `main` will always contain the working scripts.

- [manual](https://ekleinod.gitlab.io/bash-scripts)
- [issues](https://gitlab.com/ekleinod/bash-scripts/-/issues)

## Copyright

Copyright 2021-2024 Ekkart Kleinod <ekleinod@edgesoft.de>

These scripts are free software: you can redistribute them and/or modify
them under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

These scripts is distributed in the hope that they will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with these scripts.  If not, see <https://www.gnu.org/licenses/>.
